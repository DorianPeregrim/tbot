package tbot

type InlineKeyboardButton struct {
	Text 		string `json:"text"`
	Url 		string `json:"url"`
	CallbackData 	string `json:"callback_data"`
}
